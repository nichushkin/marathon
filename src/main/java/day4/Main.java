package day4;


    import java.util.Arrays;
import java.util.Random;

    /*
    1. С клавиатуры вводится число n- размер массива. Необходимо создать массив
    указанного размера и заполнить его случайными числами от 0 до 10. Затем вывести
    содержимое массива в консоль, а также вывести в консоль информацию о:
    а) Длине массива
    б) Количестве чисел больше 8
    в) Количестве чисел равных 1
    г) Количестве четных чисел
    д) Количестве нечетных чисел
    е) Сумме всех элементов массива
     */
    public class Main {
        public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("what size for array ");
//        int size = scanner.nextInt();
//        int[] array = new int[size];
//        for (int i = 0; i < array.length; i++) {
//            array[i] = (int) (Math.random() * 10);
//        }
//        System.out.println(Arrays.toString(array));
//        System.out.println(array.length);
//        int moreThan8 = 0;
//        int equal1 = 0;
//        int even = 0;
//        int odd = 0;
//        int sum = 0;
//        for (int i = 0; i < array.length; i++) {
//            if (array[i] > 8) {
//                moreThan8++;
//            }
//            if (array[i] == 1) {
//                equal1++;
//            }
//            if (array[i] % 2 == 0) {
//                even++;
//            }
//            if (array[i] % 2 != 0) {
//                odd++;
//            }
//            sum += array[i];
//        }
//        System.out.println("> 8 : " + moreThan8);
//        System.out.println("= 1 : " + equal1);
//        System.out.println("even: " + even);
//        System.out.println("odd: " + odd);
//        System.out.println("sum = " + sum);

        /*
        2. Создать новый массив размера 100 и заполнить его случайными числами из
диапазона от 0 до 10000.
Затем, используя циклы for each вывести:
- наибольший элемент массива
- наименьший элемент массива
- количество элементов массива, оканчивающихся на 0
- сумму элементов массива, оканчивающихся на 0
Использовать сортировку запрещено
         */

//        int [] array2 = new int[100];
//        Random random = new Random();
//        for (int i = 0; i < array2.length; i++) {
//            array2[i] = random.nextInt(10000);
//        }
//        int max = 0;
//        int min = 10000;
//        int ends0 = 0;
//        int sumEns0 = 0;
//        for (int value: array2) {
//            if (value > max){
//                max = value;
//            }
//            if (value < min){
//                min = value;
//            }
//            if (value % 10 == 0){
//                ends0++;
//            }
//            if (value % 10 == 0){
//                sumEns0 += value;
//            }
//        }
//        System.out.println(Arrays.toString(array2));
//        System.out.println("max = " + max);
//        System.out.println("min = " + min);
//        System.out.println("ends0 = " + ends0);
//        System.out.println("sumEns0 = " + sumEns0);

        /*
        Заполнить двумерный массив (матрицу) случайными числами от 0 до 50. Размер
матрицы задать m=12, n=8(m- размерность по строкам, n- размерность по колонкам).
В консоль вывести индекс строки, сумма чисел в которой максимальна. Если таких
строк несколько, вывести индекс последней из них.
Пример сгенерированной матрицы(для простоты  m=3, n=5)
         */
//        int m = 12;
//        int n = 8;
//        int[][] matrix = new int[m][n];
//        Random random = new Random();
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[i].length; j++) {
//                matrix[i][j] = random.nextInt(50);
//            }
//        }
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[i].length; j++) {
//                System.out.print(matrix[i][j] + " ");
//            }
//            System.out.println();
//        }
//        int sum;
//        int[] resault = new int[m];
//        for (int i = 0; i < matrix.length; i++) {
//            sum = 0;
//            for (int j = 0; j < matrix[i].length; j++) {
//                sum += matrix[i][j];
//            }
//            resault[i] = sum;
//        }
//        System.out.println(Arrays.toString(resault));
//        int k = 0;
//        for (int i = 0; i < resault.length; i++) {
//            if (resault[k] <= resault[i]){
//                k = i;
//            }
//        }
//        System.out.println(k);

        /*
        4. Создать новый массив размера 100 и заполнить его случайными числами из
диапазона от 0 до 10000.
Найти максимум среди сумм трех соседних элементов. Для найденной тройки с
максимальной суммой выведите значение суммы и индекс первого элемента тройки.
         */
            int [] array4 = new int[100];
            Random random = new Random();
            for (int i = 0; i < array4.length; i++) {
                array4[i] = random.nextInt(10000);
            }
            System.out.println(Arrays.toString(array4));
            int sum = 0;
            int maxSum = 0;
            int idxMaxSum = 0;
            for (int i = 0; i < array4.length - 2; i++) {
                sum = array4[i] + array4[i+1] + array4[i+2];
                System.out.print(sum + " ");
                if (sum > maxSum){
                    maxSum = sum;
                    idxMaxSum = i;
                }
            }
            System.out.println("\n" + maxSum);
            System.out.println(idxMaxSum);
        }
    }


