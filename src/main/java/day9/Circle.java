package day9;

public class Circle extends Figure{

    private int radious;

    public Circle(int radious, String color) {
        super(color);
        this.radious = radious;
    }

    @Override
    public double area() {
        return Math.PI * Math.pow(radious, 2);
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * radious;
    }
}
