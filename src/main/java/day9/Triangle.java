package day9;

public class Triangle extends Figure{

    private int side1, side2, side3;

    public Triangle(int side1, int side2, int side3, String color) {
        super(color);
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    @Override
    public double area() {
        double halfPerimetr = perimeter() / 2;
        return Math.sqrt(halfPerimetr * (halfPerimetr - side1) * (halfPerimetr - side2) * (halfPerimetr - side3));
    }

    @Override
    public double perimeter() {
        return side1 + side2 + side3;
    }
}
