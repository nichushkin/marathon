package day5;

public class Task {
    public static void main(String[] args) {
        /*
        1. Создать класс Автомобиль (англ.  Car ), с полями “Год выпуска”, “Цвет”, “Модель”.
Создать  get и  set методы для каждого поля. Создать экземпляр класса Автомобиль,
задать сеттером каждое поле, вывести в консоль значение каждого поля геттером.
Созданный вами класс должен отвечать принципам инкапсуляции.
         */
        Car car = new Car();
        car.setColor("green");
        car.setModel("leaf");
        car.setYear(2015);
        System.out.printf("car %s color %s and year %d", car.getModel(), car.getColor(), car.getYear());

        /*
        2. Создать класс Мотоцикл (англ.  Motorbike ), с полями “Год выпуска”, “Цвет”,
“Модель”. Создать экземпляр класса Мотоцикл, с помощью конструктора (сеттеры не
использовать). Придерживаться принципов инкапсуляции и использовать ключевое
слово  this . Геттером получить год выпуска, цвет, модель, вывести значения в
консоль
         */

        Motobike mootobike = new Motobike(2015, "black", "nissan");

        System.out.printf("\ncar model %s year %d color %s", mootobike.getModel(), mootobike.getYear(), mootobike.getColor());
    }
}

