package day7;

public class Player {
    private int stamina;
    private static final int MAX_STAMINA = 100;
    private static final int MIN_STAMINA = 0;
    private static int countPlayers;

    public Player(int stamina) {
        this.stamina = stamina;
        if (countPlayers < 6){
            countPlayers++;
        }
    }

    public int getStamina() {
        return stamina;
    }

    public void run() {
        if (stamina == 0){
            return;
        }
        stamina--;
        if (stamina == 0){
            System.out.println("Player exhausted");
            countPlayers--;
        }
    }

    public static void info(){
        if (countPlayers < 6){
            System.out.println("We need players " + (6 - countPlayers) );
        } else if(countPlayers == 6) {
            System.out.println("Let`s play");
        } else {
            System.out.println("We have reserve: " + (countPlayers - 6));
        }
    }

}
