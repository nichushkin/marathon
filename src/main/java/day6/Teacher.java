package day6;

import java.util.Random;

public class Teacher {
    private String name;
    private String subject;

    public Teacher(String name, String subject) {
        this.name = name;
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        subject = subject;
    }

    public void evaluate(Student student){
        Random random = new Random();
        int mark = random.nextInt(4) + 2;
        String printMark = "";
        switch (mark){
            case 2: printMark = "the worst";
            break;
            case 3: printMark = "bad";
            break;
            case 4: printMark = "good";
            break;
            case 5: printMark = "excelent";
            break;
        }
        System.out.printf("Teacher: %s, evaluate student: %s, by subject: %s, on mark: %s%n", this.name,
                student.getName(), this.subject, printMark);
    }
}
