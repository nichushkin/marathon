package day6;

public class Motobike {
    private int year;
    private String color;
    private String model;

    public Motobike(int year, String color, String model){
        this.color = color;
        this.year = year;
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public String getColor() {
        return color;
    }

    public String getModel() {
        return model;
    }

    public void info (){
        System.out.println("This is a bike");
    }

    public int yearDifference(int year){
        return year - this.year;
    }
}

